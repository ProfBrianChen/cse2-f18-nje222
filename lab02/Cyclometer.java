//Nicole ElChaar, 9/6/18, CSE002
//This program will calcuate the distance of each trip based on the number of cycles of a wheel

public class Cyclometer {
  //main method required for every Java program
  public static void main(String[] args) {
    //input data
    
    
int secsTrip1=480; //time elapsed Trip 1
int secsTrip2=3220; //time elapsed Trip 2
int countsTrip1=1561; //number of spins of the wheel for trip 1
int countsTrip2=9037; //number of spins of the wheel for trip 2

double wheelDiameter=27.0, 
        PI=3.14159, //value of pi for circle calculations
        feetPerMile=5280,  //value for converstion between feet and miles
        inchesPerFoot=12,   //value for inch to feet conversions
        secondsPerMinute=60;  //value for converting seconds to minutes
double distanceTrip1, distanceTrip2, totalDistance;  //creates a double for each trip distance

System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");

System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");

//calculate distances and store values
distanceTrip1=countsTrip1*wheelDiameter*PI; //determines the distance in INCHES from the number of spins of the wheel
distanceTrip1/=inchesPerFoot*feetPerMile; //gives distance in MILES
distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //gives trip 2 distance in MILES
totalDistance=distanceTrip1+distanceTrip2;

//Print output data
System.out.println("Trip 1 was "+distanceTrip1+ " miles.");
System.out.println("Trip 2 was "+distanceTrip2+ " miles.");
System.out.println("The total distance was "+totalDistance+ " miles.");

    
  } //end of main method
} //end of class

