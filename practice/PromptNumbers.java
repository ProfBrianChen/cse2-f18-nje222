import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class PromptNumbers2 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.print( "Enter the number of inputs: " );
		boolean isIntValid = scan.hasNextInt();
		while (isIntValid == false) { //if an integer is not entered
			String removal = scan.next(); //collect the user's terrible input
			System.out.print("Error, please enter an integer: ");
			isIntValid = scan.hasNextInt();
		} //end integer input check for number of hands
		int[] input = new int[scan.nextInt()];


		for (int i = 0; i < input.length; i++) {
			System.out.print("\nEnter integer " + (i+1) + ": ");
			isIntValid = scan.hasNextInt();
			while (isIntValid == false) { //if an integer is not entered
				String removal = scan.next(); //collect the user's terrible input
				System.out.print("Error, please enter an integer: ");
				isIntValid = scan.hasNextInt();
			}
			input[i] = scan.nextInt();
		}
		
		System.out.println("In order, the numbers you submitted were: " + Arrays.toString(input));
		System.out.print("Reversed, the numbers you submitted were: [" ); //force array formatting
		for (int i = input.length-1; i >= 0; i--) {
			System.out.print(input[i]);
			if (i != 0) {
				System.out.print(", ");
			}
		}
		System.out.print("]\n"); //force array formatting
	}
}
