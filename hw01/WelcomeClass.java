public class WelcomeClass {
  public static void main(String args[]){
    ///prints diagram of text to terminal window
    //could have separated lines with "text" + newline + "text", but this made formatting easier
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    //prints Welcome
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-N--J--E--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    //prints autobiography
    System.out.println("Hi, I'm Nicole ElChaar.  I'm studying IBE Mechanical Engineering at Lehigh.");
  }
  
}