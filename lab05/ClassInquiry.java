//Nicole ElChaar
//This program will ask the user about details of their class and ensure they enter the right types
import java.util.Scanner; //import the scanner

public class ClassInquiry {
	public static void main(String[] args) { //main method required for all Java programs
		Scanner scan = new Scanner( System.in ); //prepare scanner for use
		
		System.out.print("Enter the title of the course you are currently taking: ");
		boolean isTitleValid = scan.hasNextLine();
    while (isTitleValid == false) { //if nothing is entered
			System.out.println("Error, please enter a name.");
			isTitleValid = scan.hasNextLine();
		}
		String courseName = scan.nextLine(); //set courseName to the title of the course

		
		System.out.printf("Enter the department name for %s: ", courseName);		
		boolean isCNValid = scan.hasNextLine();
		while (isCNValid == false) {
			System.out.println("Error, please enter a name.\n");
      isCNValid = scan.hasNextLine();
		}
		String deptName = scan.nextLine(); //set deptName to the department of the course
		
		System.out.printf("Enter the number of times %s meets each week: ", courseName);
		boolean isMeetValid = scan.hasNextInt();
		while (isMeetValid == false) {
      String collector = scan.nextLine();
      System.out.println("Error, please enter an integer.\n");
      isMeetValid = scan.hasNextInt();
		}
    int meetTimes = scan.nextInt();
    
    /* if ( meetTimes < 0 ) {
      System.out.println("Error, please enter a positive number.");
      isMeetValid = scan.hasNextInt();
		  while (isMeetValid == false) {
        System.out.println("Error, please enter an integer.\n");
        isMeetValid = scan.hasNextInt();
		   }
    }
    */
    
		
		System.out.println("Enter the hour the class starts in the format 'xx': ");
    boolean isHourValid = scan.hasNextInt();
		while (isHourValid == false) {
      String collector = scan.nextLine();
      System.out.println("Error, please enter a number less than 24. ");
      isHourValid = scan.hasNextInt();
		}
    int hour = scan.nextInt();
    /* while (hour > 24 || isHourValid == false || hour > 0) {
      String collector = scan.nextLine();
      isHourValid = false;
      System.out.println("Error, please enter a number less than 24. ");
      isHourValid = scan.hasNextInt();
      if (isHourValid == true) {
        hour = scan.nextInt();
      }
    }
    */
  
    System.out.println("Enter the minute the class starts in the format 'xx': ");
    boolean isMinValid = scan.hasNextInt(); //intermediate variable
		while (isMinValid == false) {
      String collector = scan.nextLine();
      System.out.println("Error, please enter a number less than 60. ");
      isMinValid = scan.hasNextInt();
		}
    int minute = scan.nextInt();
    /*while (minute > 60 || isMinValid == false || minute > 0) {
      String collector = scan.nextLine();
      isMinValid = false;
      System.out.println("Error, please enter a number less than 60. ");
      isMinValid = scan.hasNextInt();
      if (isMinValid == true) {
        minute = scan.nextInt();
      }
    }    
    */
    
    System.out.printf("Enter the professor's name for %s: ", courseName);		
		boolean isProfValid = scan.hasNext();
		while (isProfValid == false) {
      String collector = scan.next();
			System.out.println("Error, please enter a name.\n");
      isProfValid = scan.hasNext();
		}
		String prof = scan.next(); //set prof to the professor of the course
    
		System.out.printf("Enter the number of students in %s: ", courseName);
		boolean isStuNumValid = scan.hasNextInt();
		while (isStuNumValid == false) {
      String collector = scan.next();
      System.out.println("Error, please enter an integer.\n");
      isStuNumValid = scan.hasNextInt();
		}
    int stuNum = scan.nextInt();
    
    System.out.println("\nCourse name: " + courseName);
    System.out.println("Department name: " + deptName);
    System.out.println("Professor name: " + prof);
    System.out.println("Number of students: " + stuNum);
    System.out.println("Meetings a week: " + meetTimes);
    System.out.println("Meet time: " + hour + ":" + minute);
    
    
    
    
	} //end main method
} //end class
