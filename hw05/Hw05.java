//Nicole ElChaar
//(odds still aren't matching accepted values)

import java.util.Scanner;

public class Hw05 {
	public static void main(String[] args) {
		Scanner scan = new Scanner ( System.in );

		System.out.print( "Enter the number of hands: " );
		boolean isHandValid = scan.hasNextInt();
		while (isHandValid == false) { //if an integer is not entered
			String removal = scan.next(); //collect the user's terrible input
			System.out.print("Error, please enter an integer: ");
			isHandValid = scan.hasNextInt();
		} //end integer input check for number of hands
		int numHands = scan.nextInt(); //set numHands to the input integer
		int numHandsRemain = numHands; //set a decreasing value for the number remaining

		int fourKind=0, threeKind=0, twoPair=0, onePair=0; //create variables for storing the total

		//random number generation for each hand
		int card1=0, card2=0, card3=0, card4=0, card5=0;
		int value1=0, value2=0, value3=0, value4=0, value5=0; 

		while ( numHandsRemain-- > 0 ) {
			int i = 1;
			while ( i <= 5 ) {
				int r = 1 + (int)(Math.random() * (52)); //generate a random number from 1-52, including 52
				if (i == 1) {
					value1 = r; //set random number value (1-52) into each value
				}
				else if (i == 2) {
					value2 = r;
					if ( value2 == value1 ) { //ensure no duplicates
						i--;
					}
				}
				else if (i == 3) {
					value3 = r;
					if ( value3 == value2 ||  value3 == value1 ) { //ensure no duplicates
						i--;
					}
				}
				else if (i == 4) {
					value4 = r;
					if ( value4 == value3 || value4 == value2 ||  value4 == value1 ) { //ensure no duplicates
						i--;
					}
				}
				else if (i == 5) {
					value5 = r;
					if ( value5 == value4 || value5 == value3 || value5 == value2 ||  value5 == value1 ) { //ensure no duplicates
						i--;
					}
				}
				else {
					System.out.print("Value assignment error");
				}

				//lower r to a value from 1-13 to put into card#
				if ( r > 0 && r <= 13 ) { //diamonds are 1-13
				}
				else if ( r > 13 && r <= 26 ) { //clubs are 13-26
					r = r - 13; //lower the value for a standard range of 1-13 after this step
				}
				else if ( r > 26 && r <= 39 ) { //hearts are 26 to 39
					r = r - 26; //lower the value for a standard range of 1-13 after this step
				}
				else if ( r > 39 && r <= 52 ) { //spades are 39 to 52
					r = r - 39; //lower the value for a standard range of 1-13 after this step
				}
				else { //other values should not appear, but if they do, they should be addressed
					r = 0; //error value
					System.out.println("13 standardizing error");
				}

				//set card value to hold the number from 1-13
				if (i == 1) {
					card1 = r; //set random number value (1-13)
				}
				else if (i == 2) {
					card2 = r;
				}
				else if (i == 3) {
					card3 = r;
				}
				else if (i == 4) {
					card4 = r;
				}
				else if (i == 5) {
					card5 = r;
				}
				else {
					System.out.print("Card no. assignment error");
				}

				i++; //increment i up 1 for the next value in the hand
			} //end while for random card generation

			//put the card numbers in order for comparison
			//long and tedious, but no arrays messing up my grade phew
			int cardHolder = 0, j = 0; //create variables to hold card temporarily and count
			while (j++ < 4) {
				if ( card2 < card1 ) { //if card 1 is larger
					cardHolder = card1; 
					card1 = card2;
					card2 = cardHolder; //switch card 1 and card 2
				}
				if ( card3 < card2 ) {
					cardHolder = card2;
					card2 = card3;
					card3 = cardHolder; //switch card 2 and card 3
				}
				if ( card4 < card3 ) {
					cardHolder = card3;
					card3 = card4;
					card4 = cardHolder; //switch card 3 and card 4
				}
				if ( card5 < card4 ) {
					cardHolder = card4;
					card4 = card5;
					card5 = cardHolder; //switch card 4 and card 5
				}
			} //end ordering

			//find four and three of a kind
      //was in a while loop so it could be broken out of if doubles were found, remedied by making two and one pair else ifs
			//bool h = true;
			//while ( h == true ) {
				if (card1 == card2 && card1 == card3) {
					if (card1 == card4) {
						fourKind++; //increment fourKind if four values are equal
						//System.out.println("four kind found");
						//break;
					}
					else {
						threeKind++; //increment threeKind if three values are equal
						//System.out.println("three kind found 1st loop");
					}
				}

				else if (card2 == card3 && card2 == card4) {
					if (card2 == card5) {
						fourKind++; //increment fourKind if four values are equal
						//System.out.println("four kind found");
						//break;
					}
					else {
						threeKind++; //increment threeKind if three values are equal
						//System.out.println("three kind found 1st loop");
					}
				}

				else if (card3 == card4 && card3 == card5) {
					threeKind++; //increment threeKind if three values are equal
					//System.out.println("three kind found last loop");
				}
				//find one and two pairs
				else if (card1 == card2 &&(card3 == card4||card4 == card5)) {
					twoPair++;
					//System.out.println("two pair found 1st loop");
					//break;
				}
				else if (card2 == card3&&card4 == card5) {
					twoPair++;
					//System.out.println("two pair found 2nd loop");
					//break;
				}					
				else if (card1 == card2||card2==card3||card3==card4||card4==card5){
					onePair++;
					//System.out.println("one pair found");

				}
				else {
					//System.out.println("nothing found");
				}
				//h == false;
				//System.out.println("h changed");
			//}
			//System.out.println("# Cards remaining: " + numHandsRemain);
			//System.out.println( card1 + " " + card2 + " " + card3 + " " + card4 + " " + card5 + "\n"); //print each card for individual checks
		} //end random number generation, repeat for next hand

		//print results
		double probFourKind = (double) fourKind / (double) numHands; //calculate probability
		double probThreeKind = (double) threeKind / (double) numHands; //calculate probability
		double probTwoPair = (double) twoPair / (double) numHands;
		double probOnePair = (double) onePair / (double) numHands;

    //four of a kind should be 0.0002, three of a kind should be 0.002, two pair should be 0.04, and one pair should be 0.04 yikes
		System.out.println("Number of loops: " + numHands);
		System.out.printf("Probability of Four-of-a-Kind: %6.3f\n", probFourKind);
		System.out.printf("Probability of Three-of-a-Kind: %6.3f\n", probThreeKind);
		System.out.printf("Probability of Two-Pair: %6.3f\n", probTwoPair);
		System.out.printf("Probability of One-Pair: %6.3f\n", probOnePair);

		//System.out.printf("# Four-of-a-Kind: %6.0f\n", (float) fourKind);
		//System.out.printf("# Three-of-a-Kind: %6.0f\n", (float) threeKind);
		//System.out.printf("# Two Pair: %6.0f\n", (float) twoPair);
		//System.out.printf("# One Pair: %6.0f\n", (float) onePair);

	} //end main method
} //end class
