//Nicole ElChaar
//This program finds the volume of a pyramid
//from the base and height values

//load the scanner
import java.util.Scanner;

public class Pyramid {
  //set up main method
  public static void main(String[] args) {
    //create new scanner
    Scanner scanner = new Scanner(System.in);
    
    //ask for input from the user for length of the square
    System.out.println("What is the length along the base of the pyramid? ");
      double lengthBase = scanner.nextDouble();
    
    //ask for input for height
    System.out.println("What is the height of the pyramid? ");
      double height = scanner.nextDouble();
    
    //calculate the volume of the pyramid
    double volume = lengthBase * lengthBase * height / 3;
    
    //print the volume of the pyramid
    System.out.println("The volume of the regular square pyramid is " + volume + " units cubed.");
    
  }
}

