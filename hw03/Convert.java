//Nicole ElChaar
//This program calculates the number of cubic miles of rainfall
//by converting from inches and acreage

//import scanner for use in inputs
import java.util.Scanner;

public class Convert {
  public static void main(String[] args) {
    //prepare scanner to accept inputs
    Scanner scanner = new Scanner(System.in);
    
    //ask the user how many acres of land were affected by hurricane precepitation
    System.out.println("Enter the affected area in acres: ");
      double numAcres = scanner.nextDouble();    
    
    //ask the user how many inches of rain
    System.out.println("Enter the rainfall in the affected area in inches: ");
      double numRain = scanner.nextDouble();
    
   //create necessary variables
    double area, depth, cubeMiles;
    
    //convert and calcuate cubic miles
    area = numAcres * 0.001562501;
    depth =  numRain * 0.00001578283;
    cubeMiles = area * depth;
    
    //display the number of acres
    System.out.println("Quanity of rain: " + cubeMiles + " cubic miles");
    
  }
}