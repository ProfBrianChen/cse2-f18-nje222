//Nicole ElChaar, Section 311, due 10/23/2018
//receives input from user to determine the size of the grid
//an x will be plotted from the spaces in between *s

import java.util.Scanner;

public class EncryptedX {
	public static void main(String[] args) {
		//
		Scanner scan = new Scanner ( System.in );

		System.out.print( "Enter the size of the grid: " );
		boolean isInputValid = false;
		boolean isNumValid = false;
		int input = 0; //set input integer
		while (isInputValid == false || isNumValid == false) { //if an integer is not entered
			isInputValid = scan.hasNextInt();
			if (isInputValid == false) {
				String removal = scan.next(); //collect the user's terrible input
				System.out.print("Error, please enter an integer from 1 to 100: ");
			}
			if (isInputValid == true) {
				input = scan.nextInt();
				if (input > 0 && input <= 100) {
					isNumValid = true;
				}
				else {
					isNumValid = false;
					System.out.print("Error, please enter an integer from 1 to 100: ");
					//String removal = scan.next();
				}
			}
		} //end integer input check for number of hands

		for (int rowNum = 0; rowNum < input; rowNum++) { //repeat for each row until input is reached
			for (int place = 0; place < input; place++) { //for the location of 
				if (place == rowNum || place == input - rowNum - 1) { 
					//print a space when the row number equals the place
					//or when it is that close to the end of the row
					System.out.print(" ");
				}
				else {
					//otherwise, print a "*"
					System.out.print("*");
				}
			}
			System.out.print("\n"); //new line for next row
		}
	}
}