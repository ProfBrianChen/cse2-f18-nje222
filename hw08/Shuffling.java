//Nicole Elchaar. Section 311. Due 11/13/18.
//This program shuffles a deck of 52 playing cards and
//returns hands until all cards are used up.
//Then the deck is reshuffled and more cards are picked.

import java.util.Scanner;
public class Shuffling{ 
	public static void main(String[] args) { 
		Scanner scan = new Scanner(System.in); 
		//suits club, heart, spade or diamond 
		String[] suitNames = {"C","H","S","D"};    
		String[] rankNames = {"2","3","4","5","6","7","8","9","10","J","Q","K","A"}; 
		String[] cards = new String[52]; 
		String[] hand = new String[5]; 
		int numCards = 5; //number of cards per hand
		int again = 1; 
		int index = 51;
		//assign card values in order
		for (int i=0; i<52; i++){ 
			cards[i]=rankNames[i%13] + suitNames[i/13]; 
			if (i == 0) {
				System.out.println("Deck Creation: ");
			}
			System.out.print(cards[i] + " "); 
		} 
		System.out.print("\nOriginal: ");
		printArray(cards); //print the ordered cards

		cards = shuffle(cards);
		System.out.print("\nShuffled: ");
		printArray(cards); //print the shuffled cards

		while(again == 1){ 
			if (index - numCards < 0) { //if out of bounds
				cards = shuffle(cards); //create a new deck
				System.out.print("\nNew Deck: ");
				printArray(cards); //print the new deck
				index = 51; //reset index to end
			}
			hand = getHand(cards,index,numCards); 
			System.out.print("\nHand: ");
			printArray(hand);
			index -= numCards;
			System.out.print("\nEnter a 1 if you want another hand drawn: "); 
			again = scan.nextInt(); 
		}  
	} //end main methods

	public static String[] getHand(String[] list, int index, int numCards) {
		String[] hand = new String[numCards];
		for (int i = 0; i < numCards; i++) { //continue until the end of the hand is reached
			hand[i] = list[index-numCards + i]; //set each element equal to the sublist
		}
		return hand;
	}

	public static String[] shuffle(String[] list) {
		for (int i = 0; i < 100; i++) {//shuffle 100 times
			//choose a random element and bubble swap
			int element = (int) (Math.random() * list.length);
			String hold; //create a temporary holder
			hold = list[element];
			list[element] = list[0];
			list[0] = hold;
		}
		return list;
	}

	public static void printArray(String[] list) {
		for (int i = 0; i < list.length; i++) {
			if (i == 0) {
				System.out.println(); //print a newline at the beginning
			}
			System.out.print(list[i] + " "); //print each element with a space
		}
	}
}
