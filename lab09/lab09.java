//Nicole ElChaar. Section 311. 11/15/18.

public class lab09 {

	public static void main(String[] args) {
		int[] array0 = {1, 2, 3, 4, 5, 6, 7, 8};
		int[] array1 = copy(array0); //use the copy method to duplicate with new pointer	
		int[] array2 = copy(array0);

		//inverter 1 array, invert without copy
		inverter(array0); 
		print(array0);
		System.out.println();

		//inverter 2, invert a copy
		inverter2(array1);
		print(array1);
		System.out.println();

		//inverter 2, copy and invert reversed copy 
		int[] array3 = inverter2(array2);
		print(array3);
		System.out.println();
	}

	public static int[] copy(int[] input) {
		int[] copy = new int[input.length];
		for (int i = 0; i < input.length; i++) { //cycle through and copy
			copy[i] = input[i];
		}
		return copy;
	}

	public static void inverter(int[] input) {
		for (int i = 0; i < Math.round(input.length/2); i++) {
			int temp = input[i]; //hold temporary value 
			input[i] = input[input.length-1-i];
			input[input.length-i-1] = temp;
		}
	}

	public static int[] inverter2(int[] input) {
		int[] inversionC = copy(input);
		for (int i = 0; i < Math.round(input.length/2); i++) {
			int temp = input[i];
			inversionC[i] = inversionC[input.length-i-1];
			inversionC[input.length-i-1] = temp; //invert with the one before last
		}
		return inversionC; //return the copied int
	}

	public static void print(int[] input) {
		for (int i = 0; i < input.length; i++) {
			System.out.print(input[i] + " "); //end each with a space
		}
	} //end print

}