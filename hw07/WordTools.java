//Nicole ElChaar, Section 311, due 10.30.18
//hw07, Word Tools
//This program allows the user to enter a string and choose from available menu options
//options are:
//c - count non WS characters, w - get num words, f - find a word/phrase,
//r - replace '!' with '.', s - remove excess spacing, and q - quit

import java.util.Scanner;

public class WordTools {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in); //create scanner object

		String text = sampleText(); //receive input text
		String menuChoice = printMenu(); //print the menu for the first time

		while (!menuChoice.equals("q")) { //if the user does not quit, continue
			if (menuChoice.equals("c")) { //non WS characters
				getNumOfNonWSCharacters(text);
			}
			else if (menuChoice.equals("w")) {
				getNumofWords(text); //num words
			}
			else if (menuChoice.equals("f")) {
				System.out.print("Enter a word/phrase to be found: "); //enter text to be parsed for
				String phrase = scan.nextLine(); //save phrase
				findText(phrase, text); //input text and 
			}
			else if (menuChoice.equals("r")) {
				replaceExclamation(text); //replace !s with .s
			}
			else if (menuChoice.equals("s")) {
				shortenSpace(text); //~kerning~ for dummies
			}
			menuChoice = printMenu(); //re-call print menu method
		}
	}

	//receive input from user
	public static String sampleText() {
		Scanner scan = new Scanner(System.in); //create scanner object
		System.out.print( "Enter a sample text: " );
		String inputText = scan.nextLine(); //receive input line
		System.out.println( "\nYou entered: " + inputText);
		return inputText;
	}

	//display and run menu options
	public static String printMenu() {
		Scanner scan = new Scanner(System.in); //create scanner object

		System.out.println("\nMENU"); //print menu
		System.out.println("c - number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");

		System.out.print("\nChoose an option: "); //print prompt

		//ensure a character from the menu is entered
		boolean isInputValid = false;
		boolean isCharValid = false;
		String hold = "a"; //set default holder for input
		while (isInputValid == false || isCharValid == false) { //if an expected character is not entered
			isInputValid = scan.hasNext();
			if (isInputValid == false) {
				String removal = scan.nextLine(); //collect the user's terrible input
				System.out.print("Error, please enter a character from the menu: ");
			}
			if (isInputValid == true) {
				hold = scan.next();
				if (hold.equals("c") || hold.equals("w") || hold.equals("f")) {
					isCharValid = true;
				}
				else if (hold.equals("r") || hold.equals("s") || hold.equals("q")) {
					isCharValid = true;
				}
				else {
					isCharValid = false;
					System.out.print("Error, please enter a character from the menu: ");
				}
			}
		} //end char input check for number of hands
		String selection = hold; //the next two lines could be one statement
		return selection; //send the menu choice back to the main method
	} //menu

	//get the number of non WS characters (c)
	public static int getNumOfNonWSCharacters(String input) {
		int count = 0; //create a counter for num of white spaces
		for (int i = 0; i < input.length(); i++) {
			if (input.charAt(i) == ' ') { //add if character is whitespace
				count++;
			}
		}
		int num = input.length() - count;
		System.out.println("Number of non-whitespace characters: " + num + "\n");
		return num;
	} //c

	//get the number of words (w)
	public static int getNumofWords(String input) {
		int count = 1; //create a counter for num of words
		for (int i = 0; i < input.length(); i++) {
			if (input.charAt(i) == ' ') { //add if character is whitespace
				count++;
			}
		}
		System.out.println("Number of words: " + count + "\n");
		return count;
	} //w

	//find a phrase (f)
	public static int findText(String parse, String input) {
		int count = 0;
		//increment through substrings to find duplicates
		for (int i = 0; i <= input.length()-parse.length(); i++) {
			int j = i + parse.length();
			if ((input.substring(i, j)).equals(parse)) {
				count++;
//				System.out.println("i: " + i + ", j: " + j);
			}
		}
		System.out.println("Instances of \"" + parse + "\": " + count);
//		System.out.println("length of parse: " + parse.length());
		return count;
	} //f

	//replace ! (r)
	public static String replaceExclamation(String input) {
		String updatedString = input.replace('!', '.');
		System.out.println("Edited text: " + updatedString + "\n");
		return updatedString;
	}

	//shorten spaces (s)
	public static String shortenSpace(String input) {
		String updatedString = input;
		//ensures triples and above are found
		for (int i = 0; i < updatedString.length()-1; i++) {
			if (updatedString.charAt(i) == ' ' && updatedString.charAt(i+1) == ' ') {
				updatedString = updatedString.replace("  ", " ");
				i--; //check against i once the space is removed
			}
		}
		System.out.println("Edited text: " + updatedString + "\n");
		return updatedString;
	} //s

} //end class body
