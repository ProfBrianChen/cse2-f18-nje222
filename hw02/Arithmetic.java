//This program will calculate the cost of each item before tax and after taxes
//Numbers will be rounded to the nearest cent
public class Arithmetic {
  //main method required for every Java program
  public static void main(String[] args) {
    int numPants = 3; //Number of pairs of pants
    double pantsPrice = 34.98; //Cost per pair of pants
    int numShirts = 2; //Number of sweatshirts
    double shirtPrice = 24.99;  //Cost per shirt
    int numBelts = 1; //Number of belts
    double beltPrice = 33.99; //Cost per belt
    double paSalesTax = 0.06; //PA tax rate

    //declare all needed costs for storing later
    double totalCostPants, totalCostShirts, totalCostBelts;
    double salesTaxPants, salesTaxShirts, salesTaxBelts;
    double totalCostNoTax, totalSalesTax, totalPaid;
    
    //calcuate total cost of each item
    totalCostPants = numPants*pantsPrice;
    totalCostShirts = numShirts*shirtPrice;
    totalCostBelts = numBelts*beltPrice;
    
    //calculate sales tax for each item
    salesTaxPants = totalCostPants*paSalesTax;
    salesTaxShirts = totalCostShirts*paSalesTax;
    salesTaxBelts = totalCostBelts*paSalesTax;
    
    //calculate total cost without tax
    totalCostNoTax = totalCostPants + totalCostShirts + totalCostBelts;
    //calculate total sales tax
    totalSalesTax = salesTaxPants + salesTaxShirts + salesTaxBelts;
    //add total sales tax to total cost without tax (for transaction cost)
    totalPaid = totalCostNoTax + totalSalesTax;
    
    //change to the appropriate amount of digits for tax values
    //***the displayed numbers are imperfect because it does not account
    //for rounding, it just cuts off the last digits***
    salesTaxPants = (int)(salesTaxPants*100)/100.0;
    salesTaxShirts = (int)(salesTaxShirts*100)/100.0;
    salesTaxBelts = (int)(salesTaxBelts*100)/100.0;

    //print the cost of each item and its tax
    System.out.println("The cost for pants is $"+totalCostPants+", and the tax on pants is $"+salesTaxPants+".");
    System.out.println("The cost for shirts is $"+totalCostShirts+", and the tax on shirts is $"+salesTaxShirts+".");
    System.out.println("The cost for belts is $"+totalCostBelts+", and the tax on belts is $"+salesTaxBelts+".");
    
    //change to the appropriate number of digits with int
    totalCostNoTax = (int)(totalCostNoTax*100)/100.0;
    totalSalesTax = (int)(totalSalesTax*100)/100.0;
    totalPaid = (int)(totalPaid*100)/100.0;
    //***the displayed numbers are imperfect because it does not account
    //for rounding, it just cuts off the last digits***
    
    //print the total cost without tax to the command window
    System.out.println("The total cost without tax is $"+totalCostNoTax+".");
    System.out.println("The total cost of tax is $"+totalSalesTax+".");
    System.out.println("The total paid in the transaction is $"+totalPaid+".");
    
    
    
    
    
    
  }
}
