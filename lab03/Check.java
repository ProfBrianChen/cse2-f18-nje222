//
//

//import scanner for use in inputs
import java.util.Scanner;

public class Check {
  //main method required for every Java program
  public static void main(String[] args) {
    //prepares program to accept inputs from Scanner
    Scanner myScanner = new Scanner (System.in);
    
    //prompt the user for the original cost of the check
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    
    //makes Scanner objects have the nextInt() method
    double checkCost = myScanner.nextDouble();
    
    //prompt the user for preferred tip percentage and convert it to a decimal
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100;
    
    //prompt the user for the number of people that ate
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    //print the output
    double totalCost;
    double costPerPerson;
    int dollars, //whole dollar amount of cost
        dimes, //10s
        pennies; //1s
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople; //=(int) costPerPerson
    
    dollars = (int) costPerPerson; //get dollar amount
    dimes = (int) (costPerPerson * 10) % 10; //dimes -> return remainder
    pennies = (int) (costPerPerson * 100) %10; //pennies -> return remainder
    
    //print the total printed
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);
    
  } //end of main method
} //end of class

