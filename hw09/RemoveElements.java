//Nicole Elchaar. Section 311. Due 11/27/18.
/*
This program removes particular elements
and returns a new array.
*/

import java.util.Scanner;

public class RemoveElements {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int num[] = new int[10];
		int newArray1[];
		int newArray2[];
		int index;
		int target;
		String answer = "";

		do{
			//get a random array
			System.out.println("Random input 10 ints [0-9]");
			num = randomInput(10, 9);
			String out = "The original array is: ";
			out += listArray(num);
			System.out.println(out);
		
			//delete a particular index
			System.out.print("Enter the index to remove: ");
			index = validInput(); //get an integer from 0-9 for index
			newArray1 = delete(num, index);
			String out1 = "The output array is ";
			out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
			System.out.println(out1);

			//remove a particular integer
			System.out.print("Enter the target value: ");
			target = validInput(); //ensure target is from 0-9
			newArray2 = remove(num,target);
			String out2="The output array is: ";
			out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
			System.out.println(out2);

			System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
			answer=scan.next();
		} while(answer.equals("Y") || answer.equals("y"));
	} //end main method

	//print array
	public static String listArray(int num[]){
		String out="{";
		for(int j = 0; j < num.length; j++){
			if(j > 0){
				out += ", ";
			}
			out += num[j];
		}
		out += "} ";
		return out;
	}

	//create random array
	public static int[] randomInput(int length, int range) {
		int[] randomArray = new int[length];
		for (int i = 0; i < length; i++) {
			randomArray[i] = (int) (Math.random() * (range+1));
		}
		return randomArray;
	}

	//get valid input
	public static int validInput() {
		Scanner scan = new Scanner(System.in);
		boolean isInputValid = false;
		boolean isNumValid = false;
		int input = 0; //set input integer
		do {
			isInputValid = scan.hasNextInt();
			if (isInputValid == false) {
				String removal = scan.next(); //get rid of input
				isNumValid = false;
				System.out.print("Error, enter a value from 0 to 9: ");
			}
			if (isInputValid == true) {
				input = scan.nextInt();
				if (input >= 0 && input < 10) {
					isNumValid = true;
				}
				else {
					isNumValid = false;
					System.out.print("Error, enter a value from 0 to 9: ");
				}
			}
		} while (isInputValid == false || isNumValid == false); //end input check
		return input;
	}
	
	//remove a particular index
	public static int[] delete(int[] list, int pos) {
		int[] shortList = new int[list.length-1]; //make shorter by 1
		for (int i = 0; i < shortList.length; i++) {
			//skip over removed index in new array
			if (i < pos) {
				shortList[i] = list[i];
			}
			else { //skip index at pos
				shortList[i] = list[i + 1];
			}
		}
		return shortList;
	}

	//remove a certain input
	public static int[] remove(int[] list, int val) {
		int count = 0;
		int accelerator = 0; //have a value to increment past index
		for (int i = 0; i < list.length; i++) {
			if (list[i] == val) { //count the number of inputs
				count++;
			}
		}
		int[] newList = new int[list.length - count]; //smaller new array
		for (int i = 0; i < newList.length; i++) {
			while (list[i + accelerator] == val) {
				accelerator++; //continue until index is not equal to val
			}
			newList[i] = list[i + accelerator];
		}
		return newList;
	}
	
}
