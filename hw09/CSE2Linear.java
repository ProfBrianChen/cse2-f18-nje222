//Nicole Elchaar. Section 311. Due 11/27/18.
/*
This program accepts input grades in ascending order,
does a binary search, shuffles, and completes a linear
search through the array.
*/

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear {
	//main method
	public static void main(String[] args) {
		//Prompt for 15 ints in ascending order
		int[] grades = prompt(15);
		for (int i = 0; i < grades.length; i++) {
			System.out.print("" + grades[i] + " "); //print the inputs
		}
		
		//binary search
		System.out.println("\nEnter a grade for binary search");
		int[] checks = prompt(1); //ensure search term is within range
		int key = checks[0];
		int[] biSearch = search(grades, key);
		if (biSearch[0] != -1) {
			System.out.println("" + key + " was found at index " + biSearch[0] + " after " + biSearch[1] + " iterations.");
		}
		else {
			System.out.println("" + key + " was not found after " + biSearch[1] + " iterations.");
		}
		
		//shuffle
		shuffle(grades);
		System.out.println("Scrambled: ");
		for (int i = 0; i < grades.length; i++) {
			System.out.print("" + grades[i] + " "); //print the inputs
		}
		
		//linear search
		System.out.println("\nEnter a grade for linear search");
		int[] lchecks = prompt(1);
		int lkey = lchecks[0];
		int[] lSearch = linearSearch(grades, lkey);
		if (lSearch[0] != -1) {
			System.out.println("" + lkey + " was found at index " + lSearch[0] + " after " + lSearch[1] + " iterations.");
		}
		else {
			System.out.println("" + lkey + " was not found after " + lSearch[1] + " iterations.");
		}
		
	}
	
	//prompt for integers
	public static int[] prompt(int length) {
		//error if not integer, not 0-100, or not >= last int
		Scanner scan = new Scanner(System.in);
		int[] grades = new int[length];
		int input = -1; //set input integer
		for (int i = 0; i < grades.length; i++) {
			if (i == 0) {
				System.out.print("Enter grade: ");
			}
			else {
				System.out.printf("Enter grade %d: ", i+1);
			}
			boolean isInt = false;
			boolean isWithinRange = false;
			boolean isAscending = false;
			while (!isInt || !isWithinRange || !isAscending) { //if proper integer is not entered
				isInt = scan.hasNextInt();
				if (isInt) {
					input = scan.nextInt();
					if (input >= 0 && input <= 100) {
						isWithinRange = true;
						if (i > 0) {
							if (input >= grades[i-1]) {
								isAscending = true;
							}
							else {
								isAscending = false;
//								i--; //decrement to copy over
								System.out.print("Error, please enter a higher integer: ");
							}
						}
						else {
							isAscending = true;
						}
					}
					else {
						isWithinRange = false;
//						i--;
						System.out.print("Error, please enter an integer from 0 to 100: ");
					}
				}
				else { //if not an integer
					String removal = scan.next(); //collect the user's terrible input
//					i--;
					System.out.print("Error, please enter an integer: ");
				}
			} //end integer input check
			grades[i] = input;
		} //end for loop
//		System.out.println("returned");
		return grades;
	}

	//binary search
	public static int[] search(int[] list, int key) {
		//create indices for max and mid
		int min = 0;
		int max = list.length-1;
		int count = 0;
		int location = -1; //default value if not found
		int[] bsearch = new int[2];

		while (max >= min) {
			int center = (min+max)/2; //round to higher
			if (list[center] == key) {
				location = center;
				count++;
				bsearch[0] = location; //return index found at index 0
				bsearch[1] = count; //return count at index 2
				return bsearch;
			}
			else if (list[center] > key) {
				max = center - 1; //move to upper half
			}
			else if (list[center] < key) {
				min = center + 1; //move to lower half
			}
			count++; //increment for number of counts
		}
		bsearch[0] = location; //return index found at index 0
		bsearch[1] = count; //return count at index 2
		System.out.println("not found");
		return bsearch;
	}
	
	//shuffle with java.util.Random
	public static void shuffle(int[] list) {
		for (int i = 0; i < 100; i++) {//shuffle 100 times
			//choose a random element and bubble swap
			Random rand = new Random();
			int element = (rand.nextInt(list.length));
			int hold; //create a temporary holder
			hold = list[element];
			list[element] = list[0];
			list[0] = hold;
		}
	}
	
	//linear search
	public static int[] linearSearch(int[]list, int key) {
		int[] lSearch = new int[2];
		int count = 0;
		int location = -1;
		for (int i = 0; i < list.length; i++) {
			if (list[i] == key) {
				location = i;
				count++;
				break; //break after first find
			}
			else {
				count++;
			}
		}
		lSearch[0] = location;
		lSearch[1] = count;
		return lSearch;
	}
	
}
