
import java.util.Scanner; //import scanner
import java.util.Random; //import random int generator

public class SentenceMethods {
	public static void main(String[] args) {
		//initialize imported stuff
		Scanner scan = new Scanner(System.in);
		Random rand = new Random();

		int random = rand.nextInt(10);
		String adjective1 = Adjective(rand.nextInt(10)); //save random adj 1
		String adjective2 = Adjective(rand.nextInt(10)); //save random adj 2
		String subject = Subject(rand.nextInt(10)); //save random sub
		String verb = Verb(rand.nextInt(10)); //save random verb
		String adjective3 = Adjective(rand.nextInt(10)); //save random verb
		String object = Object(rand.nextInt(10)); //save random verb

		//print
		System.out.print("The " + adjective1 + " " + adjective2 + " " + subject + verb);
		System.out.print("the " + adjective3 + " " + object + ".\n");

		int numSentences = rand.nextInt(5); //generate a random number of sentences
		while (numSentences > 0) {
			String actionSentence = ActionSentence(subject, rand.nextInt(10)); //new action sentence
			System.out.println(actionSentence); //print action sentence
			numSentences--;
		}
		System.out.println(Conclusion(subject));
	}
	public static String Adjective(int adj) {
		String string = "";
		switch (adj) {
		case 1:
			string = "quick";
			break;
		case 2:
			string = "dyanmic";
			break;
		case 3:
			string = "collosal";
			break;
		case 4:
			string = "old";
			break;
		case 5:
			string = "weird";
			break;
		case 6:
			string = "strong";
			break;
		case 7:
			string = "incredible";
			break;
		case 8:
			string = "strange";
			break;
		case 9:
			string = "masculine";
			break;
		default:
			string = "unnecessary";
			break;
		}
		return string;
	}
	public static String Subject(int sub) {
		String string = "";
		switch (sub) {
		case 1:
			string = "philanthropist ";
			break;
		case 2:
			string = "astronaut ";
			break;
		case 3:
			string = "demon ";
			break;
		case 4:
			string = "taxi driver ";
			break;
		case 5:
			string = "pianist ";
			break;
		case 6:
			string = "judge ";
			break;
		case 7:
			string = "fox ";
			break;
		case 8:
			string = "crimminal ";
			break;
		case 9:
			string = "student ";
			break;
		default:
			string = "lawyer ";
			break;
		}
		return string;
	}
	public static String Verb(int ver) {
		String string = "";
		switch (ver) {
		case 1:
			string = "kicked ";
			break;
		case 2:
			string = "sliced ";
			break;
		case 3:
			string = "saved ";
			break;
		case 4:
			string = "kidnapped ";
			break;
		case 5:
			string = "corroborated with ";
			break;
		case 6:
			string = "drove ";
			break;
		case 7:
			string = "sneezed on ";
			break;
		case 8:
			string = "caressed ";
			break;
		case 9:
			string = "cursed ";
			break;
		default:
			string = "commanded ";
			break;
		}
		return string;
	}
	public static String Object(int obj) {
		String string = "";
		switch (obj) {
		case 1:
			string = "aliens";
			break;
		case 2:
			string = "hornets";
			break;
		case 3:
			string = "lemons";
			break;
		case 4:
			string = "prisoners";
			break;
		case 5:
			string = "chameleons";
			break;
		case 6:
			string = "broomsticks";
			break;
		case 7:
			string = "keychains";
			break;
		case 8:
			string = "jeans";
			break;
		case 9:
			string = "fingertips";
			break;
		default:
			string = "credit cards";
			break;
		}
		return string;
	}

	public static String ActionSentence(String sub, int it) {
		Random rand = new Random();
		String sentence = "The " + sub + "used "; //have intial section of sentence
		if (it > 4) {
			sub = "It ";
			sentence = sub + "used ";
		}
		sentence += Object(rand.nextInt(10)) + " to ";
		sentence += Verb(rand.nextInt(10));
		sentence += Object(rand.nextInt(10)) + " at the ";
		sentence += Adjective(rand.nextInt(10)) + " ";
		sentence += Object(rand.nextInt(10)) + ".";

		return sentence;
	}
	
	public static String Conclusion (String sub) {
		Random rand = new Random();
		String sentence = "That " + sub; //have intial section of sentence
		
		sentence += Verb(rand.nextInt(10)) + "her ";
		sentence += Object(rand.nextInt(10)) + ".";

		return sentence;
	}

}
