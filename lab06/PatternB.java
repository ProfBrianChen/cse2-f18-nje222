//Nicole ElChaar, Section 311, 10/11/18
//Will print:
//1 2 3
//1 2 
//1

import java.util.Scanner;
public class PatternB {
	public static void main(String[] args) {
		//
		Scanner scan = new Scanner ( System.in );

		System.out.print( "Enter the number of rows: " );
		boolean isInputValid = false;
		boolean isNumValid = false;
		int input = 0; //set input integer
		while (isInputValid == false || isNumValid == false) { //if an integer is not entered
			isInputValid = scan.hasNextInt();
			if (isInputValid == false) {
				String removal = scan.next(); //collect the user's terrible input
				System.out.print("Error, please enter an integer from 1 to 10: ");
				//isInputValid = scan.hasNextInt();
			}
			if (isInputValid == true) { //if it is an integer
				input = scan.nextInt();
				if (input > 0 && input <= 10) {
					isNumValid = true; //verify it is from 1-10
				}
				else {
					isNumValid = false; //if not from 1-10
					System.out.print("Error, please enter an integer from 1 to 10: ");
					//String removal = scan.next();
				}
			}
		} //end integer input check for number of hands

		for (int rowNum = input; rowNum >= 1; rowNum--) { //maximum with the number of rows
			for (int increase = 1; increase <= rowNum; increase++) { //increase print number until row number is reached
				System.out.print(increase + " ");
			}
			System.out.print("\n");
		}
	}
}