//Nicole ElChaar, Section 311, 10/11/18
//Will print:
//3 2 1 
//2 1 
//1 

import java.util.Scanner;
public class PatternD {
	public static void main(String[] args) {
		//
		Scanner scan = new Scanner ( System.in );

		System.out.print( "Enter the number of rows: " );
		boolean isInputValid = false;
		boolean isNumValid = false;
		int input = 0; //set input integer
		while (isInputValid == false || isNumValid == false) { //if an integer is not entered
			isInputValid = scan.hasNextInt();
			if (isInputValid == false) {
				String removal = scan.next(); //collect the user's terrible input
				System.out.print("Error, please enter an integer from 1 to 10: ");
				//isInputValid = scan.hasNextInt();
			}
			if (isInputValid == true) { //if an integer
				input = scan.nextInt();
				if (input > 0 && input <= 10) { //ensure it is from 1-10
					isNumValid = true;
				}
				else {
					isNumValid = false;
					System.out.print("Error, please enter an integer from 1 to 10: ");
					//String removal = scan.next();
				}
			}
		} //end integer input check for number of hands

		for (int rowNum = input; rowNum >= 1; rowNum--) { //start at max num of rows for value
			for (int increase = rowNum; increase >= 1; increase--) { //decrease measurement
				System.out.print(increase+" "); //add spacing
			}
			System.out.print("\n");
		}
	}
}