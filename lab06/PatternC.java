//Nicole ElChaar, Section 311, 10/11/18
//Will print:
//   1
//  21
// 321
//4321

import java.util.Scanner;
public class PatternC {
	public static void main(String[] args) {
		//
		Scanner scan = new Scanner ( System.in );

		System.out.print( "Enter the number of rows: " );
		boolean isInputValid = false;
		boolean isNumValid = false;
		int input = 0; //set input integer
		while (isInputValid == false || isNumValid == false) { //if an integer is not entered
			isInputValid = scan.hasNextInt();
			if (isInputValid == false) {
				String removal = scan.next(); //collect the user's terrible input
				System.out.print("Error, please enter an integer from 1 to 10: ");
				//isInputValid = scan.hasNextInt();
			}
			if (isInputValid == true) { //if input is integer
				input = scan.nextInt();
				if (input > 0 && input <= 10) { //ensure between 1 and 10
					isNumValid = true;
				}
				else {
					isNumValid = false; //repeat if not an integer between 1 and 10
					System.out.print("Error, please enter an integer from 1 to 10: ");
					//String removal = scan.next();
				}
			}
		} //end integer input check for number of hands

		for (int rowNum = 1; rowNum <= input; rowNum++) { //start at 1
			for (int increase = rowNum; increase <= input; increase++) {
				System.out.print(" "); //start at max row number and decrease spacing
			}
			for (int increase = rowNum; increase >= 1; increase--) {
				System.out.print(increase); //start at maximum and print until min is reached
			}
			System.out.print("\n");
		}
	}
}