//Nicole ElChaar
//This program will generate a number between 1 and 52 corresponding to a card
//and will print the randomly generated card as text

public class CardGenerator {
  //standard main method for all Java programs
  public static void main(String [] args) {
    double r = 1 + (int)(Math.random() * (52)); //generate a random number from 1-52, including 52
    String s; //create a string for the suit
    
    //create if/elseif statements to determine the suit and change the value
    if ( r > 0 && r < 13 ) { //diamonds are 1-13
      s = ("diamonds");
    }
    else if ( r > 13 && r < 26) { //clubs are 13-26
      s = ("clubs");
      r = r - 12; //lower the value for a standard range of 1-13 after this step
    }
    else if ( r > 26 && r < 39) { //hearts are 26 to 39
      s = ("hearts");
      r = r - 25; //lower the value for a standard range of 1-13 after this step
    }
    else if ( r > 39 && r < 52) { //spades are 39 to 52
      s = ("spades");
      r = r - 38; //lower the value for a standard range of 1-13 after this step
    }
    else { //other values should not appear, but if they do, they should be addressed
      s = ("Error"); //if there is an error in the random number generator, fix it
      r = 0;
    }
    
    //print according to the generated number
    System.out.print("You picked the ");
    if ( r == 1 ) {
      System.out.print("ace "); //add ace to string if the number is 1
    }
    else if ( r == 11) {
      System.out.print("jack "); //add jack to string if the number is 11
    }
    else if ( r == 12) {
      System.out.print("queen "); //add queen to string if the number is 12
    }
    else if ( r == 13) {
      System.out.print("king "); //add king to string if the number is 13
    }
    else {
      r = (int)r;
      System.out.printf("%1.0f ", r); //truncate the decimals for a single digit and print the digit if it is not a face card
    }
    
     System.out.print("of " + s + ".\n"); //print the chosen suit and finish the statement
  }
}