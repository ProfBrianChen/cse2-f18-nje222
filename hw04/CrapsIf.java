//Nicole Elchaar
//use if statements to determine the slang from the game Craps
//from two dice rolls

import java.util.Scanner; //import scanner

public class CrapsIf {
  //standard main method for all java programs
  public static void main(String[] args) {
    //prepares the program to accept new inputs from the keyboard
    Scanner scanner = new Scanner (System.in);
    //declare variables bc Java doesn't like me when I put them in if statements
    int d1 = 0, d2 = 0;
    
    //prompt the user for the preferred method of dice input
    System.out.print("Would you like to enter your own dice values? (input 'yes' or 'no') ");
    String response = scanner.next();
    if (response.equals("yes")) {
      //prompt the user for the roll of dice 1
      System.out.printf("Enter the roll of dice 1: ");
      d1 = scanner.nextInt();
      //if the dice roll is valid, prompt the user for the roll of dice 2
      if (d1 >= 1 && d1 <= 6) {
        System.out.printf("Enter the roll of dice 2: ");
        d2 = scanner.nextInt();
        if (d2 >= 1 && d2 <= 6) {
        } //end d2 verification and continue
        else {
          System.out.println("Roll 2 is invalid, try again.");
          d1 = 0;
          d2 = 0;
        } //end d2 check
      } //end d2 prompt and check
      else {
        //if the roll of d1 is invalid, leave error response
        System.out.println("Roll 1 is invalid, try again.");
        d1 = 0;
        d2 = 0;
      } //end d1 and d2 determinants
    } //end 'yes' response if dice are input
    
    else if (response.equals("no")){
      //if the user wants to cast dice, generate random numbers and print them
      d1 = 1 + (int)(Math.random() * 6);
      d2 = 1 + (int)(Math.random() * 6);
      System.out.printf("First roll: %d", d1);
      System.out.printf("\nSecond roll: %d\n", d2);
    }
    
    else {
      System.out.println("Invalid response.  Enter 'yes' or 'no', try again.");
    } //end 'no' response with error listed
    
    
    //Begin slang determination
    int sum = d1 + d2;
    //Print default statement
    if (sum == 0) {
    }
    else {
      System.out.print("You rolled ");
    } //end if/else statement for 0 rolls
    
    //start by determining the sum of the roll
    if (sum == 2) {
      System.out.printf("Snake Eyes.");
    } //end if sum is 2
    else if (sum == 3) {
      System.out.printf("Ace Deuce.");
    } //end if sum is 3
    else if (sum == 4) {
      if (d1 == 2) {
        System.out.printf("Hard Four.");
      }
      else {
        System.out.printf("Easy Four.");
      }
    } //end if sum is 4
    else if (sum == 5) {
        System.out.printf("Fever Five.");
      } //end if sum is 5
    else if (sum == 6) {
      if (d1 == 3) {
        System.out.printf("Hard Six.");
      }
      else {
        System.out.printf("Easy Six.");
      }
    } //end if sum is 6
    else if (sum == 7) {
      System.out.printf("Seven Out.");
    } //end if sum is 7
    else if (sum == 8) {
      if (d1 == 4) {
        System.out.printf("Hard Eight.");
      }
      else {
        System.out.printf("Easy Eight.");
      }
    } //end if sum is 8
    else if (sum == 9) {
      System.out.printf("Nine.");
    } //end if sum is 9
    else if (sum == 10) {
      if (d1 == 5) {
        System.out.printf("Hard Ten.");
      }
      else {
        System.out.printf("Easy Ten.");
      }
    } //end if sum is 10
    else if (sum == 11) {
      System.out.printf("Yo-leven.");
    } //end if sum is 11
    else if (sum == 12) {
      System.out.printf("Boxcars.");
    } //end if sum is 12
    else {
      System.out.printf("Error.");
    } //end if error exists
    System.out.printf("\n"); //add a return following the program
  }
}