//Nicole ElChaar
//This program will use switch statements to determine the slang 
//from the game Craps of two dice rolls

import java.util.Scanner; //import scanner

public class CrapsSwitch {
  //main method necessary for all Java programs
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in); //prepare the program to accept new inputs from the keyboard    
    //declare variables bc Java doesn't like me when I put them in switch statements
    int d1 = 0, d2 = 0;
    
    //prompt the user for the preferred method of dice input
    System.out.print("Would you like to enter your own dice values? (input 'yes' or 'no') ");
    String response = scanner.next();
    
    switch ( response ) {
      case "yes":
      //prompt the user for the roll of dice 1
      System.out.printf("Enter the roll of dice 1: ");
      d1 = scanner.nextInt();
      //if the dice roll is valid, prompt the user for the roll of dice 2
      switch ( d1 ) {
        case 1:
          System.out.printf("Enter the roll of dice 2: ");
          d2 = scanner.nextInt();
          //determine if roll 2 is invalid
          switch ( d2 ) {
            case 1:
            break;
            case 2:
            break;
            case 3:
            break;
            case 4:
            break;
            case 5:
            break;
            case 6:
            break;
            default:
              System.out.println("Roll 2 is invalid, try again.");
            break;
          } //end d2 check and continue
        break; //end d2 prompt and check
        case 2:
          System.out.printf("Enter the roll of dice 2: ");
          d2 = scanner.nextInt();
          //determine if roll 2 is invalid
          switch ( d2 ) {
            case 1:
            break;
            case 2:
            break;
            case 3:
            break;
            case 4:
            break;
            case 5:
            break;
            case 6:
            break;
            default:
              System.out.println("Roll 2 is invalid, try again.");
            break;
          } //end d2 check and continue
          break;
          case 3:
          System.out.printf("Enter the roll of dice 2: ");
          d2 = scanner.nextInt();
          //determine if roll 2 is invalid
          switch ( d2 ) {
            case 1:
            break;
            case 2:
            break;
            case 3:
            break;
            case 4:
            break;
            case 5:
            break;
            case 6:
            break;
            default:
              System.out.println("Roll 2 is invalid, try again.");
            break;
          } //end d2 check and continue
        break; //end d2 prompt and check
        case 4:
          System.out.printf("Enter the roll of dice 2: ");
          d2 = scanner.nextInt();
          //determine if roll 2 is invalid
          switch ( d2 ) {
            case 1:
            break;
            case 2:
            break;
            case 3:
            break;
            case 4:
            break;
            case 5:
            break;
            case 6:
            break;
            default:
              System.out.println("Roll 2 is invalid, try again.");
            break;
          } //end d2 check and continue
        break; //end d2 prompt and check
        case 5:
          System.out.printf("Enter the roll of dice 2: ");
          d2 = scanner.nextInt();
          //determine if roll 2 is invalid
          switch ( d2 ) {
            case 1:
            break;
            case 2:
            break;
            case 3:
            break;
            case 4:
            break;
            case 5:
            break;
            case 6:
            break;
            default:
              System.out.println("Roll 2 is invalid, try again.");
            break;
          } //end d2 check and continue
        break; //end d2 prompt and check
        case 6:
          System.out.printf("Enter the roll of dice 2: ");
          d2 = scanner.nextInt();
          //determine if roll 2 is invalid
          switch ( d2 ) {
            case 1:
            break;
            case 2:
            break;
            case 3:
            break;
            case 4:
            break;
            case 5:
            break;
            case 6:
            break;
            default:
              System.out.println("Roll 2 is invalid, try again.");
              d2 = 0;
            break;
          } //end d2 check and continue
        break; //end d2 prompt and check
        default:
          //if the roll of d1 is invalid, leave error response
          System.out.println("Roll 1 is invalid, try again.");
          d1 = 0;
        break; //end d1 and d2 determinants
      } //end d1 and d2 choices if valid
    break; //end 'yes' response if dice are input
    
    case "no":
      //if the user wants to cast dice, generate random numbers and print them
      d1 = 1 + (int)(Math.random() * 6);
      d2 = 1 + (int)(Math.random() * 6);
      System.out.printf("Roll 1: %d", d1);
      System.out.printf("\nRoll 2: %d\n", d2);
    break;
    
    default:
      System.out.println("Invalid response.  Enter only '1' or '0', try again.");
    break; //end 'no' response with error listed
    } //end choosing dice switch statement
    
    
    
    //determine slang term
    int sum = d1 + d2; //have sum as a seperate variable for easy calling
    //Print default statement
    System.out.print("You rolled ");
    //start by determining the sum of the roll
    switch ( sum ) {
      case 2:
        System.out.printf("Snake Eyes.");
      break; //end if sum is 2
      case 3:
        System.out.printf("Ace Deuce.");
      break; //end if sum is 3
      case 4:
        //switch statement to determine hard or soft 4
        switch ( d1 ) {
          case 2:
            System.out.printf("Hard Four.");
          break;
          default:
          System.out.printf("Easy Four.");
        } //end switch for 4s
      break; //end if sum is 4
      case 5:
        System.out.printf("Fever Five.");
      break; //end if sum is 5
      case 6:
        //switch statement to determine hard or soft 6
        switch ( d1 ) {
          case 3:
            System.out.printf("Hard Six.");
          break;
          default:
          System.out.printf("Easy Six.");
        } //end switch for 6s
      break; //end if sum is 6
      case 7:
      System.out.printf("Seven Out.");
      break; //end if sum is 7
      case 8:
        //switch statement to determine hard or soft 8
        switch ( d1 ) {
          case 4:
            System.out.printf("Hard Eight.");
          break;
          default:
          System.out.printf("Easy Eight.");
        } //end switch for 8s
      break; //end if sum is 8
      case 9:
        System.out.printf("Nine.");
      break;
      case 10:
        //switch statement to determine hard or soft 10
        switch ( d1 ) {
          case 5:
            System.out.printf("Hard Ten.");
          break;
          default:
          System.out.printf("Easy Ten.");
        } //end switch for 10s
      break; //end if sum is 10
      case 11:
       System.out.printf("Yo-leven.");
      break; //end if sum is 11
      case 12:
        System.out.printf("Boxcars.");
      break; //end if sum is 12
      default:
       System.out.printf("Error, try again.");
      break;//end if error exists
    } //end switch statement
    System.out.printf("\n"); //add a return following the program
    
    
    
    
  } //end main method
} //end classes